// Copyright (c) 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
var storage = chrome.storage.local
/**
 * Get the current URL.
 *
 * @param {function(string)} callback - called when the URL of the current tab
 *   is found.
 */
function getCurrentTabUrl(callback) {
  // Query filter to be passed to chrome.tabs.query - see
  // https://developer.chrome.com/extensions/tabs#method-query
  var queryInfo = {
    active: true,
    currentWindow: true
  };

  chrome.tabs.query(queryInfo, function(tabs) {
    // chrome.tabs.query invokes the callback with a list of tabs that match the
    // query. When the popup is opened, there is certainly a window and at least
    // one tab, so we can safely assume that |tabs| is a non-empty array.
    // A window can only have one active tab at a time, so the array consists of
    // exactly one tab.
    var tab = tabs[0];

    // A tab is a plain object that provides information about the tab.
    // See https://developer.chrome.com/extensions/tabs#type-Tab
    var url = tab.url;

    // tab.url is only available if the "activeTab" permission is declared.
    // If you want to see the URL of other tabs (e.g. after removing active:true
    // from |queryInfo|), then the "tabs" permission is required to see their
    // "url" properties.
    console.assert(typeof url == 'string', 'tab.url should be a string');

    callback(url);
  });

  // Most methods of the Chrome extension APIs are asynchronous. This means that
  // you CANNOT do something like this:
  //
  // var url;
  // chrome.tabs.query(queryInfo, function(tabs) {
  //   url = tabs[0].url;
  // });
  // alert(url); // Shows "undefined", because chrome.tabs.query is async.
}
function main(){
	
	setInterval(function() {
	arrCat = JSON.parse(localStorage.getItem("checked"));
	//alert(arrCat[1].url);
	arrTags = localStorage.getItem("tags");
	$('#itemsList').empty();
	ParseTags();
	arrCat.forEach(function(element,index,array){
		//alert(element.url+"akbar");
		console.log("AAAAAA");
		ParseCategory(element.url);
	})
	},60000);
}

function ParseCategory(url){
	var query = $.ajax(url);
	query.done(function(html){
		var $Tbody=$(html).find("tbody")
		var name = "";
		var url = "";
		var img="";
		var k =0 
		//$Tbody.find('h3').each(function()
		regularTable = $(html).find("table[id='offers_table']");
		$(regularTable).find("img.fleft").each(function()
		{
			k+=1;
			if (k>1){
				return false;
			};
			name =$(this).attr('alt');
			// var name = $(this).find('a').attr("strong").text;
			url = $(this).parent("a").attr("href");
			console.log(name+"NAME");
			console.log(url + "URL");
			img = $(this).attr("src");
			console.log(img+"IMAGE URL");
			$('#itemsList').append("<li> <img src="+img +"><a href="+url+">"+name+"</a></li>")
		})
		
	})
}
function ParseTags(){
	tags = JSON.parse(localStorage.getItem("tags"));
	tags.forEach(function(element,i,arr){
		ParseCategory("http://olx.ua/list/q-"+element.url);
	})
}

function FirstParse(){
	arrCat = JSON.parse(localStorage.getItem("checked"));
	//alert(arrCat[1].url);
	arrTags = localStorage.getItem("tags");
	$('#itemsList').empty();
	ParseTags();
	arrCat.forEach(function(element,index,array){
		//alert(element.url+"akbar");
		console.log("AAAAAA");
		ParseCategory(element.url);
	})
}
document.addEventListener('DOMContentLoaded', function() {
  main();
  FirstParse();
  getCurrentTabUrl(function(url) {
    }, function(errorMessage) {
      renderStatus('Cannot  connect to Heroes3.tv  ' + errorMessage);
    });
  });